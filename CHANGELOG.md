# Changelog
All notable changes to this project are documented in this file. The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2024-01-25

### Added
- Function `get_from_cz_ares_ee` for data via ARES Base Register (Economic Entities).
- Function `get_from_cz_ares_vr` for entities via ARES Public Register.
- Set default value to `None` for optional attributes of data classes.
- Enum type `MemberRoleType` for attribute `Member.role`.
- Enum type `RegisterType` for resolution of register.
- Dataclass `VerifiedCompanyPublicRegister`.

### Changed
- New ARES source from https://ares.gov.cz.
- VerifiedCompany.address: `str` to VerifiedCompany.address: `Optional[str]`.
- LegalPerson.representative: `Optional[NaturalPerson]` to LegalPerson.representatives: `List[NaturalPerson]`.
- Change `Member.role` from description value to slug - enum like value.
- Do not use value `---` in date from VIES.

### Removed
- Remove parameters `service_url` and `ns` from function `get_from_cz_ares`.

## [1.2.1] - 2022-09-22

### Fixed
- Fix install test/data.

## [1.2.0] - 2022-09-22

### Added
- Add legal form from ARES.
- Add parsing of copany entities.

### Fixed
- Fix double city in district.

## [1.1.1] - 2022-08-26

### Changed
- For city use ARES element "Nazev_obce". For Prague use the number of the city district in "Nazev_mestske_casti".

## [1.1.0] - 2022-08-24

### Added
- Add support of AT - Austria.

## [1.0.1] - 2022-06-17

### Changed
- Downgrade zeep version to >= 3.3.1.

## [1.0.0] - 2022-06-16

### Added
- Verify VAT ID/REG number implementation for ARES and VIES registry.


[Unreleased]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/2.0.0...main
[2.0.0]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/1.2.1...2.0.0
[1.2.1]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/1.1.1...1.2.0
[1.1.1]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/1.0.1...1.1.0
[1.0.1]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.nic.cz/utils/verify-vat-number/-/compare/539c40216f...1.0.0
